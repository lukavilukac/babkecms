@extends('admin.layout')
@section('content')
<div>
<h1>
    Blog categories <small>(Insert, update, delete)</small>
</h1>
</div>
<hr>
<div id="validation_message"></div>
<div>
    <fieldset id="save_blog_category">
        <input type="hidden" name="id">
        <div style="width: 20vw;">
            <label for="title">blog categories title</label>
            <input class="form-control col-md-3" type="text" name="title" placeholder="title">
        </div>
        <div>&nbsp;<button class="btn btn-success" id="save_blog_category_confirm">save</button></div>
    </fieldset>
</div>
<h3>list of categories</h3>
<div id="blog_categories_table" class="table-responsive">
</div>
@stop

@section('scripts')
<script src="{{ asset ("/js/admin/blog_module/categories.js") }}" type="text/javascript"></script>
@stop