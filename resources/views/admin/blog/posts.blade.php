@extends('admin.layout')
@section('head')
<link href="{{ asset("Trumbowyg-master/dist/ui/trumbowyg.min.css") }}" rel="stylesheet" type="text/css" />
@stop
@section('content')
<div>
<h1>
    Blog posts <small>Create, update, delete</small>
</h1>
</div>
<br>
<div id="validation_message"></div>
<fieldset id="save_blog_post">
<input type="hidden" name="id" value="">
<div style="width: 20vw;">
    <label for="title">Title</label>
    <input class="form-control col-md-3" type="text" name="title" placeholder="title">
</div>
<div style="width: 20vw;">
    <label for="category">Category</label>
    <select class="form-control" name="category">
      <option value="0" disabled selected>Chose category</option>
      <option value="null">uncategorized</option>
      @foreach(Menu::getBlogCategoriesForMenu() as $category)
      <option value="{{$category->id}}">{{$category->title}}</option>
      @endforeach
    </select>
</div>
<div style="width: 20vw;">
    <label for="keywords">keywords (separate with coma)</label>
    <input class="form-control col-md-3" type="text" name="keywords" placeholder="keywords">
</div>
<div style="width: 20vw;">
    <label for="representative_image">Representative Image</label>
    <button id="add_representative_image" class="btn btn-default">Chose representative image</button>
    <div id="representative_image_holder">
        <img style="max-height: 50px; max-width: 100px;" alt="img" src="blog-posts-default">
        <input type="hidden" name="representative_image">
    </div>
</div>
<div style="width: 20vw;">
    <label for="description">Description</label>
    <textarea class="form-control" name="description"></textarea>
</div>

<textarea id="blog_post_content"></textarea><!--REMOVE NAME BEFORE INTEGRATION WITH WYSWYG-->
<div style="text-align: center;">
    <button class="btn btn-success" id="save_blog_confirm">save</button>&nbsp;<button class="btn btn-danger" id="save_blog_clear">clear</button>
</div>
</fieldset>

<h3>Get blog posts by category</h3>
<div style="width: 20vw;">
<select class="form-control" id="blog_categories_list">
    <option value="0" disabled selected>Get news by category</option>
    <option value="null">uncategorized</option>
    @foreach(Menu::getBlogCategoriesForMenu() as $category)
    <option value="{{$category->id}}">{{$category->title}}</option>
    @endforeach
</select>
</div>
<div id="blog_posts_list"></div>
@stop

@section('scripts')
<script src="{{ asset("Trumbowyg-master/dist/trumbowyg.min.js") }}" type="text/javascript"></script>
<script src="{{ asset("/js/admin/blog_module/trumbowyg-config.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/js/admin/blog_module/posts.js") }}" type="text/javascript"></script>
@stop