<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>BabkeCMS</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/css/app.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/adminLTE/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("adminLTE/dist/css/skins/skin-blue.min.css") }}" rel="stylesheet" type="text/css" />
    <!--icons that are used for adminlte-->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--icons that are used for adminlte-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('head')
</head>
<body class="skin-blue">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">babke<b>CMS</b></a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <div class='logout' style='padding-top:20px; padding-right:20px;'>
                    <a style='color:white;' href="{{ url('/logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                    <strong>Logout</strong>
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..."/>
      <span class="input-group-btn">
        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
      </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">HEADER</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="treeview">
                    <a href="#"><span>Blog</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/blog-posts">Posts</a></li>
                        <li><a href="/admin/blog-categories">Categories</a></li>
                    </ul>
                </li>
                <!--<li><a data-toggle="modal" href="#folders_images_popup"><span>folder images popup</span></a></li>-->
            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!--usable in whole system image popup-->
            <div id="folders_images_popup_wrapper"></div>
            <!--usable in whole system image popup-->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- Default to the left -->
        <p>Sva prava © 2017 babke<strong>CMS</strong></p>
    </footer>

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<script src="{{ asset ("/js/app.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/adminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/js/api.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/js/helpers.js") }}" type="text/javascript"></script>
<!--modal scripts-->
@yield('scripts')
</body>
</html>