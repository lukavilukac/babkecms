//check if jquery is defined
if(typeof jQuery === 'undefined') {
    throw 'For using api jquery has to be loaded!';
}
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var Api = {
    contentType : "application/x-www-form-urlencoded; charset=utf-8",
    post: function(_url, _data, _success, _error){
        $.ajax({
            url: _url,
            type: 'POST',
            data: _data,
            contentType: this.contentType,
            success: _success,
            error: _error
        });
    },
    get: function(_url, _success, _error){
        $.ajax({
            url: _url,
            type: 'GET',
            success: _success,
            error: _error
        });
    },

    delete: function(_url, _success, _error){
        $.ajax({
            url: _url,
            type: 'POST',
            contentType: this.contentType,
            data: {_method: 'delete'},
            success: _success,
            error: _error
        });
    },
    
    fileUpload: function(_url, _data, _success, _error){
        $.ajax({
            url: _url,
            type: 'POST',
            dataType: 'json',
            data: _data,
            processData: false,
            contentType: false,
            success: _success,
            error: _error
        });
    }
};