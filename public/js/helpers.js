if (typeof babkeCMS === 'undefined'){
    var babkeCMS = {};
}

babkeCMS.responseBuilder = function (){
    
    this.error = function(divId, responseObject){
        divId.addClass('alert alert-danger');
        if(typeof responseObject === 'string'){
            divId.text(responseObject);
        }else{
            $.each(responseObject, function(key, value){
                for(i = 0; i < key.length; i++){
                    divId.text(value[i]);
                }
            });
        }
        $('html, body').animate({
            scrollTop: divId.offset().top}
        );
        
        setTimeout(function(){
            divId.removeClass('alert alert-danger');
            divId.empty();
        }, 3500);
    };
    
    this.success = function(divId, responseObject){
        divId.addClass('alert alert-success');
        divId.text(responseObject);
        $('html, body').animate({
            scrollTop: divId.offset().top}
        );
        
        setTimeout( function(){
            divId.empty();
            divId.removeClass('alert alert-success');
        }, 3500);
    };
};

babkeCMS.clearFormElements = function(id, wiswygId) {
    $(id).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'text':
            case 'textarea':
            case 'file':
            //case 'select-one':
            //case 'select-multiple':
            case 'date':
            case 'number':
            case 'tel':
            case 'email':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
                break;
            case 'select-one':
            case 'select-multiple':
                $(this).val('0');
                break;
        }
    });
    if (typeof(wiswygId) !== 'undefined'){
        $('#'+wiswygId).trumbowyg('html', '');
    }
    
};

babkeCMS.makeJsonArrayFromInput = function(input){
    function isNotEmpty(value){
        return value !== '';
    };
    var keywordsArray = input.replace(/[^a-zA-Z0-9\.,]/g, "").split(",").filter(isNotEmpty);
    res = keywordsArray.length > 0 ? JSON.stringify(keywordsArray) : '';
    return res;
};