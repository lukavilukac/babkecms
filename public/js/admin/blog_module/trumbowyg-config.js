//$.trumbowyg.svgPath = '/bower_components/trumbowyg/dist/ui/icons.svg';
$('#blog_post_content').trumbowyg({
    btnsDef: {
    // Customizables dropdowns
        /*image: {
            dropdown: ['insertImage', 'upload'],
            ico: 'insertImage'
        },*/
        babkeImages: {
            ico: 'insertImage'
        }
    },
    btns: [
        ['viewHTML'],
        ['undo', 'redo'],
        ['formatting'],
        'btnGrp-design',
        ['link'],
        ['babkeImages'],
        'btnGrp-justify',
        'btnGrp-lists',
        ['foreColor', 'backColor'],
        ['preformatted'],
        ['horizontalRule'],
        ['fullscreen'],
    ],
    plugins: {
        /*upload: {
            serverPath: '/admin/img-upload-for-news',
            fileFieldName: 'image',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },*/
        babkeImages: {
            testVar: 'John Doe',
            
        },
    },
});