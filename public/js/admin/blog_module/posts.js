if (typeof babkeCMS === 'undefined'){
    var babkeCMS = {};
}

if (typeof babkeCMS.BlogPostsModel !== 'function'){
    babkeCMS.BlogPostsModel = function(){
        this.categoryId;
        this.blogPostsCollection;
        
        this.blogPostsCollectionFactory = function (data) {
            if(!(data.constructor === Array)){
                throw 'Invalid data structure from server must, be a type of array';
            }
            return data;
        };         
    };
};

var BlogPostController = function(){
    if(typeof jQuery === 'undefined'){
        throw 'For using posts.js you need to load jquery!';
    }
    
    if(typeof Api === 'undefined'){
        throw "For using categories.js you need to load api.js";
    }
    
    //instantiating a model for this controller
    var model = new babkeCMS.BlogPostsModel();
    //instantiating modal responses for this controller
    var responses = new babkeCMS.responseBuilder();
    
    //view changing methods
    var renderBlogPostsTable = function (blogPostsCollection){
        newsListTable = '<div class="table-responsive"><table id="mytable" class="table table-bordred table-striped"><thead><th>blog post title</th><th>Edit</th><th>Delete</th></thead><tbody>';
        tableBody = '';
        for(i = 0; i < blogPostsCollection.length; i++){
            tableBody += '<tr><td style="display:none;">'+blogPostsCollection[i].id+'</td><!--id becouse of editing--><td style="display:none;">'+blogPostsCollection[i].content+'</td><!--full content becouse of editing--><td style="display:none;">'+blogPostsCollection[i].category_id+'</td><!--category becouse of editing--><td>'+blogPostsCollection[i].title+'</td><td style="display:none;">'+blogPostsCollection[i].keywords+'</td><td style="display:none;">'+blogPostsCollection[i].description+'</td><td style="display:none;">'+blogPostsCollection[i].representative_image+'</td><td><button class="edit-blog-post-button btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td><td><button class="delete-blog-post-button btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button></p></td></tr>';
        }
        newsListTable += tableBody;
        newsListTable += '</tbody></table></div>';
        $('#blog_posts_list').html(newsListTable);
    };
    //actions
    
    var getBlogPostsByCategoryId = function(categoryId){
        Api.get(
            '/admin/blog-posts-get-by-category/'+categoryId,
            function(data){
                var preparedData = model.blogPostsCollectionFactory(data);
                if(model.blogPostsCollection !== preparedData){
                   model.blogPostsCollection = preparedData;
                   renderBlogPostsTable(model.blogPostsCollection);
                }
            },
            function(msg){
                console.log(msg);
            }
        );
    };
    
    var blogPostDeleteById = function(id){
        res = confirm('Are you sure you want to delete this blog post?');
        if(res === true){
            Api.delete(
                '/admin/blog-posts-delete/'+id,
                function(msg){
                    getBlogPostsByCategoryId(model.categoryId);
                    responses.success($('#validation_message'), msg);
                    babkeCMS.clearFormElements('#save_blog_post', 'blog_post_content');
                },
                function(msg){
                    responses.error($('#validation_message'), 'ERROR, The blog post you specified does not exists in data base!');//does not work propper, should get message from server               
                }
            );
        }
    };
    
    var blogPostSave = function(data){
        Api.post(
            '/admin/blog-posts-save',
            data,
            function(msg){
                responses.success($('#validation_message'), msg);
                babkeCMS.clearFormElements('#save_blog_post', 'blog_post_content');
                if(model.categoryId !== 'undefined'){
                    $('#blog_categories_list').val(model.categoryId);
                    getBlogPostsByCategoryId(model.categoryId);
                }
            },
            function(msg){
                if('responseJSON' in msg){
                    responses.error($('#validation_message'), msg.responseJSON);
                } else {
                    console.log(msg);
                }
            }
        );
    };
    
    //events
    $('#save_blog_confirm').on('click', function(){
        keywords = babkeCMS.makeJsonArrayFromInput($('input[name=keywords]', $('#save_blog_post')).val());
        model.categoryId = $('select[name=category]', $('#save_blog_post')).val();
        var data = {
            id:                     $('input[name=id]', $('#save_blog_post')).val(),
            title:                  $('input[name=title]', $('#save_blog_post')).val(),
            category:               model.categoryId,
            keywords:               keywords,
            representative_image:   $('input[name=representative_image]', $('#save_blog_post')).val(),
            description:            $('textarea[name=description]', $('#save_blog_post')).val(),
            content:                $('#blog_post_content').trumbowyg('html')
        };
        blogPostSave(data);
    });
    $('#save_blog_clear').on('click', function(){
        babkeCMS.clearFormElements('#save_blog_post', 'blog_post_content');
    });
    $('#blog_categories_list').change(function(){
        var categoryId = $(this).val();
        getBlogPostsByCategoryId(categoryId);
    });
    $('body').on('click', '.delete-blog-post-button', function(){
        id = $(this).closest('tr').find('td:eq(0)').text();
        model.categoryId = $(this).closest('tr').find('td:eq(2)').text();
        blogPostDeleteById(id);
    });
    $('body').on('click', '.edit-blog-post-button', function(){
        $('input[name=id]', $('#save_blog_post')).val($(this).closest('tr').find('td:eq(0)').text());
        $('input[name=title]', $('#save_blog_post')).val($(this).closest('tr').find('td:eq(3)').text());
        $('select[name=category]', $('#save_blog_post')).val($(this).closest('tr').find('td:eq(2)').text());
        $('input[name=keywords]', $('#save_blog_post')).val($(this).closest('tr').find('td:eq(4)').text() === 'null' ? '' : JSON.parse($(this).closest('tr').find('td:eq(4)').text()));
        $('textarea[name=description]', $('#save_blog_post')).val($(this).closest('tr').find('td:eq(5)').text() === 'null' ? '' : $(this).closest('tr').find('td:eq(5)').text());
        $('input[name=representative_image]', $('#save_blog_post')).val($(this).closest('tr').find('td:eq(6)').text());
        $('#blog_post_content').trumbowyg('html', "<p>dsdfsfd</p>" /*$(this).closest('tr').find('td:eq(1)').html()*/);
        //scroll to top
        $('input[name=title]', $('#save_blog_post')).focus();
    });
}();