if (typeof babkeCMS === 'undefined'){
    var babkeCMS = {};
}

if (typeof babkeCMS.BlogCategoriesModel !== 'function'){
    babkeCMS.BlogCategoriesModel = function(){
        this.blogCategoriesCollection;
        
        this.blogCategoryCollectionFactory = function (data) {
        if(!(data.constructor === Array)){
            throw 'Invalid data structure from server must, be a type of array';
        }
        
        return data;
        }; 
    };
};

var blogCategoriesController = function () {
    if(typeof jQuery === 'undefined') {
        throw 'For using categories.js you need to load jquery!';
    }
    
    if(typeof Api === 'undefined'){
        throw "For using categories.js you need to load api.js";
    }
    
    //instantiating a model for this controller
    var model = new babkeCMS.BlogCategoriesModel();
    //instantiating modal responses for this controller
    var responses = new babkeCMS.responseBuilder();
    
    //view changing methods
    var renderCategoriesTable = function(blogCategoriesCollection){
        if (blogCategoriesCollection.length > 0) {
            var newsCategoriesTable = '<table class="table table-bordred table-striped"><thead><th>name</th><th>Edit</th><th>Delete</th></thead><tbody>';
            var tableBody = '';
            for (i = 0; i < blogCategoriesCollection.length; i++){
               tableBody += '<tr><td style="display:none;">'+blogCategoriesCollection[i].id+'</td><td>'+blogCategoriesCollection[i].title+'</td><td><button class="edit-category-button btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td><td><button class="delete-category-button btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td></tr>';
            }
            newsCategoriesTable += tableBody;
            newsCategoriesTable += '</tbody></table>';
            $('#blog_categories_table').html(newsCategoriesTable);
        }
    };
    
    //actions
    var blogCategoriesGetAll = function(){
        Api.get(
            '/admin/blog-categories-get-all',
            function(data){
                var preparedData = model.blogCategoryCollectionFactory(data);
                if(model.blogCategoriesCollection !== preparedData){
                   model.blogCategoriesCollection = preparedData;
                   renderCategoriesTable(model.blogCategoriesCollection);
                }
            },
            function(msg){
                console.log(msg);
            }
        );
    };
    
    var blogCategorySave = function (data) {
        data;
        Api.post(
            '/admin/blog-category-save',
            data,
            function(msg){
                responses.success($('#validation_message'), msg);
                babkeCMS.clearFormElements('#save_blog_category');
                blogCategoriesGetAll();
            },
            function(msg){
                if('responseJSON' in msg){
                    responses.error($('#validation_message'), msg.responseJSON);
                } else {
                    console.log(msg);
                }
            }
        );
    };
    
    var blogCateogryDeleteById = function(id){
        r = confirm('Are you sure you want to delete this cateogry?');
        if (r === true){
            Api.delete(
                '/admin/blog-category-delete/'+id,
                function(msg){
                    responses.success($('#validation_message'), msg);
                    babkeCMS.clearFormElements('#save_blog_category');
                    blogCategoriesGetAll();
                },
                function(msg){
                    responses.error($('#validation_message'), 'ERROR, The news you specified does not exists in data base!');//does not work propper, should get message from server
                }
            );
        }
    };
    
    //events
    $(window).on('load', function(){
        blogCategoriesGetAll();
    });
    
    $('#save_blog_category_confirm').on('click', function(){
        data = {
            id: $('input[name=id]', $('#save_blog_category')).val(),
            title: $('input[name=title]', $('#save_blog_category')).val()
        };
        blogCategorySave(data);
    });
    
    $('body').on('click', '.edit-category-button', function(){
        $('input[name=id]', $('#save_blog_category')).val($(this).closest('tr').find('td:eq(0)').text());
        $('input[name=title]', $('#save_blog_category')).val($(this).closest('tr').find('td:eq(1)').text());
        $('input[name=id]', $('#save_blog_category')).focus();
    });
    
    $('body').on('click', '.delete-category-button', function(){
        blogCateogryDeleteById($(this).closest('tr').find('td:eq(0)').text());       
    });
}();