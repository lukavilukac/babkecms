<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Always make route group and add namespace
|
*/

Route::get('/', function () {
    return view('welcome');
});

//full namespace of other controllers App\Http\Controllers

// Authentication Routes...
Route::group(['namespace' => 'App\Http\Controllers'], function(){
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});

//admin routes
Route::group(['namespace' => 'App\Admin\Modules', 'prefix' => 'admin', 'middleware' => ['auth', 'admin']], function()
{
    Route::get('/', function () {
        return view('admin.index');
    });
    //blog module routes (categories)
    Route::get('blog-categories', 'Blog\Controllers\CategoriesController@index');
    Route::get('blog-categories-get-all', 'Blog\Controllers\CategoriesController@getAll');
    Route::post('blog-category-save', 'Blog\Controllers\CategoriesController@save');
    Route::delete('blog-category-delete/{id}', 'Blog\Controllers\CategoriesController@deleteById');
    //blog module routes (posts)
    Route::get('blog-posts', 'Blog\Controllers\PostsController@index');
    Route::get('blog-posts-get-by-category/{categoryId}', 'Blog\Controllers\PostsController@getByCategoryId');
    Route::post('blog-posts-save', 'Blog\Controllers\PostsController@save');
    Route::delete('blog-posts-delete/{id}', 'Blog\Controllers\PostsController@deleteById');
});