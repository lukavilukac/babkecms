<?php

namespace App\Admin\Modules\Blog\Controllers;

//use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Admin\Modules\Blog\Models\BlogPosts;
class PostsController extends BaseController
{
    use ValidatesRequests;
    
    private function makeLinkFriendlyTitle($title){
        $titleForUrl = strtolower(str_replace(['š', 'đ', 'ž', 'č', 'ć','Š', 'Đ', 'Ž', 'Č', 'Ć', ' '], ['s', 'd', 'z', 'c', 'c','S', 'D', 'Z', 'C', 'C', '-'], $title));
        return $titleForUrl;
    }
    
    public function index(){
        return view('admin.blog.posts');
    }
    
    public function getByCategoryId($categoryId){
        $blogPostsList  = ($categoryId === 'null') ?
            BlogPosts::where('category_id', null)->get() :
            BlogPosts::where('category_id', $categoryId)->get();
        return response()->json($blogPostsList);
    }
    
    public function save(Request $request){
        //make default representative image
        $representativeImage = '/img/something.jpg';
        $id = $request->input('id');
        $this->validate($request,[
            'title'                 => 'string|required|unique:blog_posts,title,'.$id.',id',
            'category'              => 'string|required',
            'content'               => 'required',
            'keywords'              => 'json|nullable',
            'representative_image'  => 'string|nullable',
            'description'           => 'string|nullable'
        ]);
        $blogPost = BlogPosts::findOrNew($id);
        $blogPost->title                = $request->input('title');
        $blogPost->title_for_url        = $this->makeLinkFriendlyTitle($request->input('title'));
        $blogPost->category_id          = $request->input('category') === 'null' ? null : $request->input('category');
        $blogPost->content              = $request->input('content');
        $blogPost->keywords             = $request->input('keywords');
        $blogPost->representative_image = $request->input('representative_image') === null ? $representativeImage : $request->input('representative_image');
        $blogPost->description          = $request->input('description');
        $blogPost->save();
        return response()->json("You have submited blog post successfully!");
    }
    
    public function deleteById($id){
        $blogPosts = BlogPosts::find($id);
        if (!count($blogPosts)) {
            $message = 'ERROR, The blog post you specified does not exists in data base!';
            return response()->json($message, 422);
        }
        $blogPosts->delete();
        $message = 'You have deleted category successfuly';
        return response()->json($message);
    }
}