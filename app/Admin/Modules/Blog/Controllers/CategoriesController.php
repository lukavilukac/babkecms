<?php

namespace App\Admin\Modules\Blog\Controllers;

//use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Admin\Modules\Blog\Models\BlogCategories;

class CategoriesController extends BaseController
{
    use ValidatesRequests;
    
    private function makeLinkFriendlyTitle($title){
        $title = strtolower($title);
        $title = str_replace(['š', 'đ', 'ž', 'č', 'ć','Š', 'Đ', 'Ž', 'Č', 'Ć', ' '], ['s', 'd', 'z', 'c', 'c','S', 'D', 'Z', 'C', 'C', '-'], $title);
        return $title;
    }
    
    public function index(){
        return view('admin.blog.categories');
    }
    
    public function getAll(BlogCategories $categories){
        $categoriesCollection = $categories->all();
        return response()->json($categoriesCollection);
    }

    public function save(Request $request){
        $id = $request->input('id');
        $this->validate($request, [
            'title' => 'required|string|unique:blog_categories,title,'.$id.',id'
        ]);
        
        $categories = BlogCategories::findOrNew($id);
        $categories->title = $request->input('title');
        $categories->title_for_url = $this->makeLinkFriendlyTitle($request->input('title'));
        $categories->save();
        return response()->json('You have submited category successfuly!');
    }
    
    public function deleteById($id){
        $categories = BlogCategories::find($id);
        if (!count($categories)) {
            $message = 'ERROR, The news you specified does not exists in data base!';
            return response()->json($message, 422);
        }
        $categories->delete();
        $message = 'You have deleted category successfuly';
        
        return response()->json($message);
    }
}