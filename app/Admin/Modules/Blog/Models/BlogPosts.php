<?php

namespace App\Admin\Modules\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPosts extends Model
{
    protected $table = 'blog_posts';
    protected $fillable = ['title', 'title_for_url', 'category_id', 'content', 'keywords', 'description', 'representative_image'];
}