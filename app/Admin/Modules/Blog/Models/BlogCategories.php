<?php

namespace App\Admin\Modules\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategories extends Model
{
    protected $table = 'blog_categories';
    protected $fillable = ['title', 'title_for_url'];
}
