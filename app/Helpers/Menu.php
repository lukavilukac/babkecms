<?php
namespace App\Helpers;

use App\Admin\Modules\Blog\Models\BlogCategories;


class Menu
{
    public static function getBlogCategoriesForMenu() {
        $blogCategories = BlogCategories::all(); 
        return $blogCategories;
    }
}