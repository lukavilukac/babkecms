<?php

use Illuminate\Database\Seeder;

class UsersTableAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'milos',
            'email' => 'babic.milos22@gmail.com',
            'password' => bcrypt('test'),
            'role' => 1
        ]);
    }
}
